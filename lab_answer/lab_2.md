Soal nomor satu
Perbedaan JSON dan XML!

XML dan JSON adalah format yang sangat umum untuk pertukaran data. 
Walaupun keduanya digunakan untuk menerima data dari web server, tetapi terdapat perbedaan diantara keduanya, yaitu:
1. JSON hanya berbentuk format data sedangkan XML adalah bahasa markup.
2. JSON digunakan untuk pertukaran data ringan yang membuat komputer jauh leih mudah mengurai data yang dikirim. Sedangkan XML digunakan untuk mengangkut data dari aplikasi satu ke aplikasi lain menggunakan internet.
3. Format JSON lebih sederhana sehingga mudah dibaca sedangkan XML lebih rumit.
4. JSON berorientasi pada data sedangkan XML berorientasi pada dokumen.
5. JSON mendukung array sedangkan XML tidak. 


Soal nomor dua
Perbedaan HTML dan XML

HTML dan XML keduanya merupakan bahasa markup, tetapi memiliki beberapa perbedaan, seperti:
1. HTML berfokus pada penyajian data sedangkan XML berfokus pada transfer data.
2. HTML didorong format sedangkan XML didorong oleh konten.
3. HTML itu case insensitive sedangkan XML case sensitive.
4. HTML tidak mendukung namespaces sedangkan XML mendukung namespaces.
5. HTML tidak harus menggunakan tag penutup, sedangkan XML harus. 
6. Tag pada HTML terbatas, sedangkan tag pada XML dapat dikembangkan.



Sumber :
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
https://blogs.masterweb.com/perbedaan-xml-dan-html/