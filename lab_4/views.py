from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect

@login_required(login_url='/admin/login/')
def index(request):

    listNotes = Note.objects.all().values()
    response = {'objek' : listNotes}
    return render(request, 'lab4_index.html', response)


@login_required(login_url='/admin/login/')
def add_note(request):
    response = {}

    form = NoteForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        form.save()
        return HttpResponseRedirect('/lab-4/note-list')  
        
    response['form'] = form
    return render (request, 'lab4_form.html', response)


@login_required(login_url='/admin/login/')
def note_list(request):

    listNotes = Note.objects.all().values()
    response = {'objek' : listNotes}
    return render(request, 'lab4_note_list.html', response)

