from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers


# Create your views here.
def index(request):
    listNotes = Note.objects.all().values()
    response = {'objek' : listNotes}
    return render(request, 'lab2.html', response)

def xml(request):
    listNotes = Note.objects.all().values()
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    listNotes = Note.objects.all().values()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")